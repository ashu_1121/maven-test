package Test;
import Utility.Constants;
import Application.ConfigTestRunner;
import Application.DriverFactory;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestResult;
import org.testng.annotations.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class RegressionTests {

	public String destFile;
    public ExtentTest test;
    public ExtentHtmlReporter htmlReports;   //to generate an html file
    private static ExtentReports extent;
    private ConfigTestRunner configTestRunnerLocal;
    
    @BeforeSuite
    public void extentReportConfig(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH-mm-ss");
        destFile = Constants.reportsFilePath+"BronzeVerse"+dateFormat.format(new Date());
        File newFolder = new File(destFile);
        boolean created =  newFolder.mkdir();  //mkdir will create folder
        if(created)
            System.out.println("Folder is created !");
        else
            System.out.println("Unable to create folder");

        String destDir = "BronzeVerse"+dateFormat.format(new Date()) + ".html";
        htmlReports = new ExtentHtmlReporter(destFile + "\\" +destDir);  //to generate an html file
        extent = new ExtentReports();
        extent.attachReporter(htmlReports);
        setExtent(extent);
        htmlReports.config().setReportName("BronzeVerse");
        htmlReports.config().setTheme(Theme.STANDARD);
        htmlReports.config().setDocumentTitle("BronzeVerse Test Result");
    }
    
    @BeforeMethod
    //@Parameters({"browser"})
    public void initializeBrowser(){
        DriverFactory driverFactory = new DriverFactory();
        driverFactory.startBrowser("chrome");
    }
    
//    @Test
//    public void LoginDemo001() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC001");
//    }
//    
//    @Test
//    public void LoginDemo011() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC011");
//    }
//    
//    @Test
//    public void LoginDemo012() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC012");
//    }
//    
//    @Test
//    public void LoginDemo013() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC013");
//    }
//    
//    @Test
//    public void LoginDemo014() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC014");
//    }
    
//    @Test
//    public void InviteUserToBronzeverse004() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC004");
//    }
//    
//    @Test
//    public void InviteUserToBronzeverse041() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC041");
//    }
//    
//    @Test
//    public void InviteUserToBronzeverse042() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC042");
//    }
//    
//    @Test
//    public void InviteUserToBronzeverse043() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC043");
//    }
//    
//    @Test
//    public void InviteUserToBronzeverse044() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC044");
//    }
//    
//    @Test
//    public void InviteUserToBronzeverse045() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC045");
//    }
//
    
    @Test
    public void UserRegistrationBySuperAdmin005() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC005");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin501() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC501");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin502() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC502");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin503() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC503");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin504() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC504");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin505() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC505");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin506() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC506");
    } 
    
    @Test
    public void UserRegistrationBySuperAdmin507() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC507");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin508() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC508");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin051() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC051");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin052() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC052");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin053() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC053");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin054() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC054");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin055() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC055");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0051() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0051");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0052() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0052");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0053() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0053");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0054() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0054");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0055() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0055");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0056() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0056");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0057() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0057");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0058() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0058");
    }
    
    @Test
    public void UserRegistrationBySuperAdmin0059() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC0059");
    }
    
//    
//    @Test(enabled = false)
//    public void UserRegistrationBySuperAdmin005() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC005");
//    }
    
//    @Test
//    public void UserRegistration002() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC002");
//    }
//
//    @Test
//    public void UserRegistration0021() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0021");
//    }
//
//    @Test
//    public void UserRegistration0022() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0022");
//    }
//
//    @Test
//    public void UserRegistration0023() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0023");
//    }
//
//    @Test
//    public void UserRegistration0024() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0024");
//    }
//
//    @Test
//    public void UserRegistration0025() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0025");
//    }
//
//    @Test
//    public void UserRegistration0026() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0026");
//    }
//
//    @Test
//    public void UserRegistration0027() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0027");
//    }
//
//    @Test
//    public void UserRegistration0028() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0028");
//    }
//
//    @Test
//    public void UserRegistration0029() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC0029");
//    }
//
//    @Test
//    public void UserRegistration0291() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00291");
//    }
//
//    @Test
//    public void UserRegistration0292() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00292");
//    }
//
//    @Test
//    public void UserRegistration0293() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00293");
//    }
//
//    @Test
//    public void UserRegistration0294() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00294");
//    }
//
//    @Test
//    public void UserRegistration0295() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00295");
//    }
//
//    @Test
//    public void UserRegistration0296() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00296");
//    }
//
//    @Test
//    public void UserRegistration0297() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00297");
//    }
//
//    @Test
//    public void UserRegistration0298() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00298");
//    }
//
//    @Test
//    public void UserRegistration0299() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC00299");
//    }
    
    @Test(enabled = false)
    public void UserOnboarding006() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC006");
    }
    
    @Test(enabled = false)
    public void CreateAnnouncement007() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC007");
    }
	
    @AfterMethod
    public void fnCloseBrowser(ITestResult testResult){
        if(testResult.getStatus()== ITestResult.FAILURE){
            String name ="Test_Fail_ScreenShot";
            try {
                getConfigTestRunnerLocal().getChildTest().log(Status.FAIL, "Test is failed" + getConfigTestRunnerLocal().getChildTest().addScreenCaptureFromPath(getConfigTestRunnerLocal().screenShotName(name)));
            }catch (Exception exc){
                exc.printStackTrace();
            }
            getConfigTestRunnerLocal().getBaseAction().setCellData("Fail",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        }  else if(testResult.getStatus() == ITestResult.SKIP){
            test.log(Status.SKIP,testResult.getThrowable());
            getConfigTestRunnerLocal().getBaseAction().setCellData("Skip",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        }
        getConfigTestRunnerLocal().getBaseAction().setCellData("Pass",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        if(getConfigTestRunnerLocal().driver!=null)
            getConfigTestRunnerLocal().driver.quit();
    }
    
    @AfterSuite
    public void End_SetUp(){
        getConfigTestRunnerLocal().getExtent().flush();
       
    }


    public ConfigTestRunner getConfigTestRunnerLocal() {
        return configTestRunnerLocal;
    }

    public void setConfigTestRunnerLocal(ConfigTestRunner configTestRunnerLocal) {
        this.configTestRunnerLocal = configTestRunnerLocal;
    }

    public ExtentReports getExtent() {
        return extent;
    }

    @SuppressWarnings("static-access")
	public void setExtent(ExtentReports extent) {
        this.extent = extent;
    }

}
