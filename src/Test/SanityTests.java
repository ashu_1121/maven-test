package Test;
import Utility.Constants;
import Application.ConfigTestRunner;
import Application.DriverFactory;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestResult;
import org.testng.annotations.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SanityTests {

	public String destFile;
    public ExtentTest test;
    public ExtentHtmlReporter htmlReports;   //to generate an html file
    private static ExtentReports extent;
    private ConfigTestRunner configTestRunnerLocal;
    
    @BeforeSuite
    public void extentReportConfig(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH-mm-ss");
        destFile = Constants.reportsFilePath+"BronzeVerse"+dateFormat.format(new Date());
        File newFolder = new File(destFile);
        boolean created =  newFolder.mkdir();  //mkdir will create folder
        if(created)
            System.out.println("Folder is created !");
        else
            System.out.println("Unable to create folder");

        String destDir = "BronzeVerse"+dateFormat.format(new Date()) + ".html";
        htmlReports = new ExtentHtmlReporter(destFile + "\\" +destDir);  //to generate an html file
        extent = new ExtentReports();
        extent.attachReporter(htmlReports);
        setExtent(extent);
        htmlReports.config().setReportName("BronzeVerse");
        htmlReports.config().setTheme(Theme.STANDARD);
        htmlReports.config().setDocumentTitle("BronzeVerse Test Result");
    }
    
    @BeforeMethod
    //@Parameters({"browser"})
    public void initializeBrowser(){
        DriverFactory driverFactory = new DriverFactory();
        driverFactory.startBrowser("chrome");
    }

    // it will take you to the configtestrunner run method, where it will check tc number with the excel sheet tc number
//    @Test(priority = 8)
//    public void BrokenLink009() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC009");
//    }
//    
//    
//    @Test(priority = 1)
//    public void LoginDemo001() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC001");
//    }
//    
//    
//    
//    @Test(priority = 2)
//    public void InviteUserToBronzeverse004() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC004");
//    }
//   
//    @Test(priority = 6)
//    public void UserRegistrationBySuperAdmin005() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC005");
//    }
//  
//    @Test(priority = 3)
//    public void UserRegistration002() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC002");
//    }
//    
//    @Test(priority = 4)
//    public void UserOnboarding006() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC006");
//    }
//    
//    @Test(priority = 5)
//    public void CreateAnnouncement007() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC007");
//    }
//    
//    @Test(priority = 7)
//    public void EditProfile008() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC008");
//    }
    
    @Test
    public void verifyToggle() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TCtoggle1");
    }
	
    @AfterMethod
    public void fnCloseBrowser(ITestResult testResult){
        if(testResult.getStatus()== ITestResult.FAILURE){
            String name ="Test_Fail_ScreenShot";
            try {
                getConfigTestRunnerLocal().getChildTest().log(Status.FAIL, "Test is failed" + getConfigTestRunnerLocal().getChildTest().addScreenCaptureFromPath(getConfigTestRunnerLocal().screenShotName(name)));
            }catch (Exception exc){
                exc.printStackTrace();
            }
            getConfigTestRunnerLocal().getBaseAction().setCellData("Fail",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        }  else if(testResult.getStatus() == ITestResult.SKIP){
            test.log(Status.SKIP,testResult.getThrowable());
            getConfigTestRunnerLocal().getBaseAction().setCellData("Skip",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        }
        getConfigTestRunnerLocal().getBaseAction().setCellData("Pass",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        if(getConfigTestRunnerLocal().driver!=null)
            getConfigTestRunnerLocal().driver.quit();
    }
    
    @AfterSuite
    public void End_SetUp(){
        getConfigTestRunnerLocal().getExtent().flush();
       
    }


    public ConfigTestRunner getConfigTestRunnerLocal() {
        return configTestRunnerLocal;
    }

    public void setConfigTestRunnerLocal(ConfigTestRunner configTestRunnerLocal) {
        this.configTestRunnerLocal = configTestRunnerLocal;
    }

    public ExtentReports getExtent() {
        return extent;
    }

    @SuppressWarnings("static-access")
	public void setExtent(ExtentReports extent) {
        this.extent = extent;
    }

}
