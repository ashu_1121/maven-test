package Test;

import org.apache.log4j.BasicConfigurator;
import org.testng.TestNG;

public class TestRunner {

	static TestNG testNG;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BasicConfigurator.configure();
		testNG = new TestNG();
		testNG.setTestClasses(new Class[] {SanityTests.class});
		testNG.run();
	}

}
