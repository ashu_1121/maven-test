package Test;
import Utility.Constants;
import Application.ConfigTestRunner;
import Application.DriverFactory;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestResult;
import org.testng.annotations.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Demo {

	public String destFile;
    public ExtentTest test;
    public ExtentHtmlReporter htmlReports;   //to generate an html file
    private static ExtentReports extent;
    private ConfigTestRunner configTestRunnerLocal;
    
    @BeforeSuite
    public void extentReportConfig(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH-mm-ss");
        destFile = Constants.reportsFilePath+"BronzeVerse"+dateFormat.format(new Date());
        File newFolder = new File(destFile);
        boolean created =  newFolder.mkdir();  //mkdir will create folder
        if(created)
            System.out.println("Folder is created !");
        else
            System.out.println("Unable to create folder");

        String destDir = "BronzeVerse"+dateFormat.format(new Date()) + ".html";
        htmlReports = new ExtentHtmlReporter(destFile + "\\" +destDir);  //to generate an html file
        extent = new ExtentReports();
        extent.attachReporter(htmlReports);
        setExtent(extent);
        htmlReports.config().setReportName("BronzeVerse");
        htmlReports.config().setTheme(Theme.STANDARD);
        htmlReports.config().setDocumentTitle("BronzeVerse Test Result");
    }
    
    @BeforeMethod
    //@Parameters({"browser"})
    public void initializeBrowser(){
        DriverFactory driverFactory = new DriverFactory();
        driverFactory.startBrowser("chrome");
    }
    
    @Test(priority = 1)
    public void UserRegistration002() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC002");
    }
    
    @Test(priority = 2)
    public void UserRegistration002D() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC002D");
    }
    
    @Test(priority = 3)
    public void LoginDemo001() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC001");
    }
    
    @Test(priority = 4)
    public void LoginDemo001D() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC001D");
    }
        
    @Test(priority = 5)
    public void InviteUserToBronzeverse004() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC004");
    }
    
    @Test(priority = 6)
    public void InviteUserToBronzeverse004D() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC004D");
    }
   
    
    @Test(priority = 7)
    public void UserRegistrationBySuperAdmin005() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC005");
    }
    
    
    @Test(priority = 8)
    public void UserOnboarding006() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC006");
    }
    
    @Test(priority = 9)
    public void CreateAnnouncement007() {
    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
		configTestRunner.setConfigTestRunner(configTestRunner);
		setConfigTestRunnerLocal(configTestRunner);
		configTestRunner.setDestFile(destFile);
		configTestRunner.run("TC007");
    }
    
    
//    @Test(priority = 6)
//    public void UserRegistrationBySuperAdmin005D() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC005D");
//    }
    

    

//    @Test(priority = 10)
//    public void UserOnboarding006D() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC006D");
//    }
    
    

    
//    @Test(priority = 12)
//    public void CreateAnnouncement007D() {
//    	ConfigTestRunner configTestRunner = new ConfigTestRunner(extent);
//		configTestRunner.setConfigTestRunner(configTestRunner);
//		setConfigTestRunnerLocal(configTestRunner);
//		configTestRunner.setDestFile(destFile);
//		configTestRunner.run("TC007D");
//    }
	
    @AfterMethod
    public void fnCloseBrowser(ITestResult testResult){
        if(testResult.getStatus()== ITestResult.FAILURE){
            String name ="Test_Fail_ScreenShot";
            try {
                getConfigTestRunnerLocal().getChildTest().log(Status.FAIL, "Test is failed" + getConfigTestRunnerLocal().getChildTest().addScreenCaptureFromPath(getConfigTestRunnerLocal().screenShotName(name)));
            }catch (Exception exc){
                exc.printStackTrace();
            }
            getConfigTestRunnerLocal().getBaseAction().setCellData("Fail",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        }  else if(testResult.getStatus() == ITestResult.SKIP){
            test.log(Status.SKIP,testResult.getThrowable());
            getConfigTestRunnerLocal().getBaseAction().setCellData("Skip",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        }
        getConfigTestRunnerLocal().getBaseAction().setCellData("Pass",getConfigTestRunnerLocal().getBaseAction().rowValue(getConfigTestRunnerLocal().getBaseAction().getTestCase().get("TC_ID"), getConfigTestRunnerLocal().getBaseAction().ColumnValue("TC_ID","TestCase")),getConfigTestRunnerLocal().getBaseAction().ColumnValue("Status","TestCase"));
        if(getConfigTestRunnerLocal().driver!=null)
            getConfigTestRunnerLocal().driver.quit();
    }
    
    @AfterSuite
    public void End_SetUp(){
        getConfigTestRunnerLocal().getExtent().flush();
       
    }


    public ConfigTestRunner getConfigTestRunnerLocal() {
        return configTestRunnerLocal;
    }

    public void setConfigTestRunnerLocal(ConfigTestRunner configTestRunnerLocal) {
        this.configTestRunnerLocal = configTestRunnerLocal;
    }

    public ExtentReports getExtent() {
        return extent;
    }

    @SuppressWarnings("static-access")
	public void setExtent(ExtentReports extent) {
        this.extent = extent;
    }

}
