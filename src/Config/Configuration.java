package Config;

import org.openqa.selenium.WebElement;

import Utility.ConfigurationReader;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

public class Configuration {


	private static final String XPATH_FILE = "field-xpath.properties";

	private Properties fieldXpath = ConfigurationReader.readPropertiesFile(XPATH_FILE);


	public Configuration() {
	}

	public static Configuration read() {
		Configuration configuration = new Configuration();
		configuration.fieldXpath = ConfigurationReader.readPropertiesFile(XPATH_FILE);
		return configuration;
	}

	public String getXpath(String formName, String fieldName) {
		String key = formName + "." + fieldName;
		return checkValueForNull(key, fieldXpath.getProperty(key));
	}

	private String checkValueForNull(String key, String value) {
		if (value == null) {
			throw new IllegalArgumentException("Key " + key + " value not found.");
		}
		return value;
	}

	public WebElement getWebElement(WebDriver driver, String formName, String name) {
		String xpath = getXpath(formName, name);
		return driver.findElement(By.xpath(xpath));
	}
	
	public List<WebElement> getWebElementes(WebDriver driver, String formName, String name) {
		List<WebElement> elm = null;
		String xpath = getXpath(formName, name);
		try {
			elm = driver.findElements(By.xpath(xpath));
		} catch (NoSuchElementException e) {
			elm = null;
		}
		return elm;
	}
}
