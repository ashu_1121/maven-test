package Utility;

public class Constants {
	public static final int AJAX_TIMEOUT = 20;
	public static final String URL = "http://test.bronzeverse.com/login";
	public static final String reportsFilePath = "resources/Reports/";
	public static final String TestDataPath = "resources/TestData/";
	public static final String driverPath = "resources/Drivers/";
	public static final int Implicit_Wait = 20;
}
