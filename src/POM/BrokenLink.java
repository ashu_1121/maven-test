package POM;


import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import Application.BaseAction;
import Application.ConfigTestRunner;

public class BrokenLink extends BaseAction {

	public void fnBrokenLinks(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("To check the broken link on page URL "+configTestRunner.getBaseAction().getTestData().get("linkUrl")));
		  String url ;
		  goToUrl(configTestRunner.getBaseAction().getTestData().get("linkUrl"));
	      List<WebElement> allURLs = driver.findElements(By.tagName("a"));
	      System.out.println("Total links on the Wb Page: " + allURLs.size());

	      //We will iterate through the list and will check the elements in the list.
	      Iterator<WebElement> iterator = allURLs.iterator();
	      while (iterator.hasNext()) {
	    	  url = iterator.next().getText();
	    	  System.out.println(url);
	    	  configTestRunner.getChildTest().log(Status.INFO,"The coming link response is"+url);
	      }
	      	     
	}
	
}
