package POM;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import Application.BaseAction;
import Application.ConfigTestRunner;
import Utility.Constants;

public class CreateAnnouncement extends BaseAction {
	
	public CreateAnnouncement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "//img[@alt='profile' and @class='user-profile-img']")
    WebElement profile;
	
	
	@FindBy(xpath = "//div[contains(text(),'Admin Account ')]")
    WebElement adminac; 
	
	
	@FindBy(xpath = "//span[contains(text(),'Events & Marketing')]")
    WebElement eventsAndmarket; 
	
	@FindBy(xpath = "//button[contains(text(),'Create New')]")
	WebElement createOpt;
	
	@FindBy(xpath="//input[@formcontrolname='title']")
	WebElement anctitle;
	
	@FindBy(xpath="//input[@formcontrolname='message']")
	WebElement description;
	
	@FindBy(xpath="//input[@formcontrolname='linkname']")
	WebElement linkName;
	
	@FindBy(xpath="//input[@formcontrolname='link']")
	WebElement link;
	
	@FindBy(xpath="(//span[@class='dropdown-btn'])[1]")
	WebElement smadrd;
	
	@FindBy(xpath="(//span[@class='dropdown-btn'])[2]")
	WebElement familydrd;
	
	@FindBy(xpath="(//span[@class='dropdown-btn'])[3]")
	WebElement subsdrd;
	
	@FindBy(xpath="//input[@id='startdate']")
	WebElement startdate;
	
	@FindBy(xpath="//input[@id='enddate']")
	WebElement enddate;
	
	@FindBy(xpath="//button[contains(text(),'Create')]")
	WebElement creatBtn;
	
	@FindBy(xpath="//button[contains(text(),'Cancel')]")
	WebElement cancelBtn;
	
	@FindBy(xpath="//button[contains(text(),'Save Draft')]")
	WebElement saveDraftBtn;
	
	@FindBy(xpath="//div[contains(text(),'Announcement updated successfully.')]/button")
	WebElement closeNoti;
	
	@FindBy(xpath = "//div[contains(text(),' Announcement already found in our record. ')]")
	WebElement FoundNoti;
	
	@FindBy(xpath="//button[contains(text(),'preview')]")
	WebElement previewBtn;
	
	public void FnCreateAnnouncement(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("To create Announcement By SuperAdmin"));
		
		if(fnWaitForVisibility(profile, 20)) {
					
			profile.click();
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on Profile Icon");
			sleep(1000);
			
			adminac.click();
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on admin account");
			sleep(1000);
			
			if(fnWaitForVisibility(eventsAndmarket, 15)) {
				eventsAndmarket.click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on the Events and Marketting Option");
				
				sleep(10000);
				
				createOpt.click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on create new button");
				sleep(1000);
				
				
				sleep(1000);
				anctitle.sendKeys(configTestRunner.getBaseAction().getTestData().get("title"));
				configTestRunner.getChildTest().log(Status.INFO, "Inserted title as "+configTestRunner.getBaseAction().getTestData().get("title"));
				
				sleep(1000);
				description.sendKeys(configTestRunner.getBaseAction().getTestData().get("description"));
				configTestRunner.getChildTest().log(Status.INFO, "Added description for announcement"+configTestRunner.getBaseAction().getTestData().get("description") );
				
				sleep(1000);
				linkName.sendKeys(configTestRunner.getBaseAction().getTestData().get("linkname"));
				configTestRunner.getChildTest().log(Status.INFO, "Added linkname as "+configTestRunner.getBaseAction().getTestData().get("linkname") );
				
				sleep(1000);
				link.sendKeys(configTestRunner.getBaseAction().getTestData().get("link"));
				configTestRunner.getChildTest().log(Status.INFO, "Added link as "+configTestRunner.getBaseAction().getTestData().get("link") );
				
				sleep(1000);
				
				
				String[] smas = configTestRunner.getBaseAction().getTestData().get("smas").split(",");
				
				executeExtJsClick(smadrd);
				scrollTillVisible(smadrd);
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on SMA dropdown");
				
				for(int i=0; i<smas.length ;i++) {
					sleep(1500);
					scrollTillVisible(getElement("//li/div[contains(text(),'"+smas[i]+"')]"));
					getElement("//li/div[contains(text(),'"+smas[i]+"')]").click();
					configTestRunner.getChildTest().log(Status.INFO, "Selected SMA is "+smas[i]);
					sleep(1000);
				}
				
				executeExtJsClick(familydrd);
				scrollTillVisible(familydrd);
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on family dropdown");
				sleep(1000);
				
				String[] families = configTestRunner.getBaseAction().getTestData().get("families").split(",");
				
				for(int i=0; i<smas.length ;i++) {
					sleep(1500);
					scrollTillVisible(getElement("//li/div[contains(text(),'"+families[i]+"')]"));
					getElement("//li/div[contains(text(),'"+families[i]+"')]").click();
					configTestRunner.getChildTest().log(Status.INFO, "Selected Family is "+families[i]);
					sleep(1000);
				}
				
				executeExtJsClick(subsdrd);
				scrollTillVisible(subsdrd);
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on Subscription dropdown");
				sleep(1000);
				
				String[] subs = configTestRunner.getBaseAction().getTestData().get("subs").split(",");
				
				for(int i=0; i<subs.length ;i++) {
					sleep(1500);
					scrollTillVisible(getElement("//li/div[contains(text(),'"+subs[i]+"')]"));
					getElement("//li[@class='multiselect-item-checkbox']/div[text()='"+subs[i]+"']").click();
					configTestRunner.getChildTest().log(Status.INFO, "Selected Subscription as "+subs[i]);
					sleep(1000);
				}
				
				sleep(1000);
				
				startdate.sendKeys(configTestRunner.getBaseAction().getTestData().get("startdate"));
				configTestRunner.getChildTest().log(Status.INFO, "Added startdate as "+configTestRunner.getBaseAction().getTestData().get("startdate") );
				sleep(1000);
				startdate.sendKeys(Keys.TAB);
				startdate.sendKeys(configTestRunner.getBaseAction().getTestData().get("sthour"));
				configTestRunner.getChildTest().log(Status.INFO, "Added startdate  hours as "+configTestRunner.getBaseAction().getTestData().get("sthour") );
				
				
				enddate.sendKeys(configTestRunner.getBaseAction().getTestData().get("enddate"));
				configTestRunner.getChildTest().log(Status.INFO, "Added End date as "+configTestRunner.getBaseAction().getTestData().get("enddate") );
				sleep(1000);
				enddate.sendKeys(Keys.TAB);
				enddate.sendKeys(configTestRunner.getBaseAction().getTestData().get("enhour"));
				configTestRunner.getChildTest().log(Status.INFO, "Added end date  hours as "+configTestRunner.getBaseAction().getTestData().get("enhour") );
				
				if(configTestRunner.getBaseAction().getTestData().get("preview").equalsIgnoreCase("preview")) {
					if(previewBtn.isEnabled()) {
						configTestRunner.getChildTest().log(Status.INFO, "Preview button is enabled");
						scrollTillVisible(previewBtn);
						sleep(1000);
						previewBtn.click();
						configTestRunner.getChildTest().log(Status.INFO, "Clicked on preview Button");
						sleep(2000);
						try {
							configTestRunner.getChildTest().log(Status.PASS,"Announcement Preview Screenshot"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("AnnouncementPreview")));
						} catch (Exception e) {
							// TODO: handle exception
						}
						
					}else {
						configTestRunner.getChildTest().log(Status.INFO, "Preview button is disable , can not click preview button");
					}
				}
				
				if(configTestRunner.getBaseAction().getTestData().get("btn").equalsIgnoreCase("save")) {
					scrollTillVisible(creatBtn);
					executeExtJsClick(creatBtn);
					configTestRunner.getChildTest().log(Status.INFO, "Clicked on the create button");
				}else if(configTestRunner.getBaseAction().getTestData().get("btn").equalsIgnoreCase("draft")) {
					scrollTillVisible(saveDraftBtn);
					executeExtJsClick(saveDraftBtn);
					configTestRunner.getChildTest().log(Status.INFO, "Clicked on the Save Draft button");
				}else {
					scrollTillVisible(cancelBtn);
					executeExtJsClick(cancelBtn);
					configTestRunner.getChildTest().log(Status.INFO, "Clicked on the cancel button");
				}
				
				
				
				try{
	                if (fnWaitForVisibility(creatBtn, Constants.AJAX_TIMEOUT) ) {
	                    configTestRunner.getChildTest().log(Status.PASS,"Announcement Created Successfully"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("AnnouncementCreated")));
	                } else if(fnWaitForVisibility(FoundNoti, Constants.AJAX_TIMEOUT)) {
	                    configTestRunner.getChildTest().log(Status.FAIL, "Announcement Not Created No message Displayed"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("AnnouncementNotCreated")));
	                }else {
	                	configTestRunner.getChildTest().log(Status.FAIL, "Announcement Not Created No message Displayed"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("AnnouncementNotCreated")));
	                }
	            }catch (Exception e){
	                e.printStackTrace();
	                try{
	                    configTestRunner.getChildTest().log(Status.FAIL, "Announcement Not Created something went wrong"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("AnnouncementNotCreatedException")));
	                }catch (Exception e1){

	                }
	            }
				
			}
					
		}
		
	}
}
