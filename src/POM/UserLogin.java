package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import Application.BaseAction;
import Application.ConfigTestRunner;
import Utility.Constants;




public class UserLogin extends BaseAction {

	public UserLogin(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//input[@id='email']") 
	WebElement email;
	
	@FindBy(xpath="//input[@id='password']") 
	WebElement password;
	
	@FindBy(xpath="//input[@id='remember_me']") 
	WebElement remember;

	@FindBy(xpath="//button[@name='signin']") 
	WebElement login;

	@FindBy(xpath="//button[@class='invite_btn btn' and contains(text(),' Invite a friend and earn more')]") 
	WebElement inviteLink;

    @FindBy(xpath="//button[contains(text(),'Continue')]")
    WebElement cont;
    
    @FindBy(xpath = "//img[@alt='profile' and @class='user-profile-img']")
    WebElement profile;
    
    @FindBy(xpath = "//div[contains(text(),'Logout')]")
    WebElement logout;
    
    @FindBy(xpath = "//div[contains(text(),'Bronzeville')]")
	WebElement bronzeville ;
	
	public Boolean loginToApplication(ConfigTestRunner configTestRunner) {
		boolean isPresence = false;
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Login To BronzeVerse"));
        configTestRunner.getChildTest().log(Status.INFO, "URL use to login into the application is "+ Constants.URL);
        
        fnWaitForVisibility(login, 30);
        if(login.isDisplayed()) {
        	email.sendKeys(configTestRunner.getBaseAction().getTestCase().get("UserName"));
        	configTestRunner.getChildTest().log(Status.INFO, "Entered UserName as"+configTestRunner.getBaseAction().getTestCase().get("UserName"));
            sleep(1000);
            
            password.sendKeys(configTestRunner.getBaseAction().getTestCase().get("Password"));
            configTestRunner.getChildTest().log(Status.INFO, "Entered Password is "+configTestRunner.getBaseAction().getTestCase().get("Password"));
            sleep(1000);
            
            executeExtJsClick(login);
            
            sleep(5000);
            try{
            	if(configTestRunner.getBaseAction().getTestCase().get("TC_ID").equalsIgnoreCase("TC006")) {
            		if (fnWaitForVisibility(bronzeville, Constants.AJAX_TIMEOUT ) ) {
                        configTestRunner.getChildTest().log(Status.PASS,"Login to the application is successful"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Login_Successfull_Onboarding")));
                    } else{
                        configTestRunner.getChildTest().log(Status.FAIL, "Login to the application is not successful."+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Login_UnSuccessfull")));
                    }
            	}else {
            		if (fnWaitForVisibility(profile, Constants.AJAX_TIMEOUT)) {
                        configTestRunner.getChildTest().log(Status.PASS,"Login to the application is successful"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Login_Successfull")));
                    } else{
                        configTestRunner.getChildTest().log(Status.FAIL, "Login to the application is not successful."+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Login_UnSuccessfull")));
                    }
            	}
                
            }catch (Exception e){
                e.printStackTrace();
                try{
                    configTestRunner.getChildTest().log(Status.FAIL, "Login to the application is not successful."+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.capture("Login_UnSuccessfull")));
                }catch (Exception e1){

                }
            }
            
        }
        return isPresence;
        
	}

    public synchronized void LogOutFromApplication(ConfigTestRunner configTestRunner) {
        sleep(5000);
        configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("LogOut From Application Verification"));
        configTestRunner.elementUtil.fnWaitForVisibility(profile,Constants.AJAX_TIMEOUT);
        configTestRunner.elementUtil.waitAndClick(profile,Constants.AJAX_TIMEOUT);
        configTestRunner.getChildTest().log(Status.INFO, "Clicked on Profile Icon");
        configTestRunner.elementUtil.fnWaitForVisibility(logout,Constants.AJAX_TIMEOUT);
        configTestRunner.elementUtil.waitAndClick(logout,Constants.AJAX_TIMEOUT);
        configTestRunner.getChildTest().log(Status.INFO, "Clicked on Logout");
        handleAlertAccept();
        
      if(configTestRunner.elementUtil.fnWaitForVisibility(login,Constants.AJAX_TIMEOUT))
        configTestRunner.getChildTest().log(Status.PASS, "User is able to log out from the application");
      else
        configTestRunner.getChildTest().log(Status.FAIL, "User is not able to log out from the application");
    }
}
