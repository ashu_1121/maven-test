package POM;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import Application.BaseAction;
import Application.ConfigTestRunner;
import Utility.Constants;

public class UserOnboardingPom extends BaseAction {
	
	public UserOnboardingPom(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div[contains(text(),'Bronzeville')]")
	WebElement bronzeville ;
	
	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	WebElement continuebtn ;
	
	@FindBy(xpath="//button[contains(text(),'Back')]")
	WebElement backbtn;
	
	@FindBy(xpath = "//input[@formcontrolname='firstName']")
	WebElement fname;
	
	@FindBy(xpath = "//input[@formcontrolname='lastName']")
	WebElement lname;
	
	@FindBy(xpath = "//select[@id='gender']")
	WebElement gender;
	
	@FindBy(xpath = "//select[@id='countrycode']")
	WebElement country;
	
	@FindBy(xpath = "//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath = "//input[@formcontrolname='birthday']")
	WebElement birthday;
	
	@FindBy(xpath = "//input[@formcontrolname='address']")
	WebElement address;
	
	@FindBy(xpath = "//button[contains(text(),'Get Started')]")
	WebElement getStarted;
	
	@FindBy(xpath = "//img[@alt='profile' and @class='user-profile-img']")
    WebElement profile;
	
	@FindBy(xpath = "//div[contains(text(),' Profile updated successfully ')]")
	WebElement SuccessMsg;
	
	public void fnOnboarding(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Useronboarding Testcase"));
		
		fnWaitForVisibility(bronzeville, 30);
		if(bronzeville.isDisplayed()) {
			String[] smas = configTestRunner.getBaseAction().getTestData().get("smaList").split(",");

			String[] family = configTestRunner.getBaseAction().getTestData().get("familyList").split(",");
			
			for (int i = 0; i < smas.length; i++) {
				sleep(1000);
				executeExtJsClick(getElement("//div[contains(text(),'" + smas[i] + "')]"));
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on " + smas[i] + " SMA ");
			}
			
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on the continue button after selecting sma");
			
			for (int i = 0; i < family.length; i++) {
				sleep(1000);
				executeExtJsClick(getElement("//div[contains(text(),'" + family[i] + "')]"));
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on " + family[i] + " Family ");
			}

			sleep(1000);
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO,"Clicked on the continue button after selecting Families ");
			
			fname.sendKeys(configTestRunner.getBaseAction().getTestData().get("fname"));
			configTestRunner.getChildTest().log(Status.INFO,"Entered first Name as " + configTestRunner.getBaseAction().getTestData().get("fname"));
			
			lname.sendKeys(configTestRunner.getBaseAction().getTestData().get("lname"));
			configTestRunner.getChildTest().log(Status.INFO,"Entered last Name as " + configTestRunner.getBaseAction().getTestData().get("lname"));
			
			sleep(1000);
			getSelectWebElementPOM(gender).selectByVisibleText(configTestRunner.getBaseAction().getTestData().get("gender"));
			configTestRunner.getChildTest().log(Status.INFO,"Selected gender is " + configTestRunner.getBaseAction().getTestData().get("gender"));
			
			birthday.sendKeys(configTestRunner.getBaseAction().getTestData().get("birthdate"));
			configTestRunner.getChildTest().log(Status.INFO,"Selected Birthdate is " + configTestRunner.getBaseAction().getTestData().get("birthdate"));
			sleep(1000);
			
			getSelectWebElementPOM(country).selectByVisibleText(configTestRunner.getBaseAction().getTestData().get("ccode"));
			configTestRunner.getChildTest().log(Status.INFO,"Selected Countrycode as " + configTestRunner.getBaseAction().getTestData().get("cc"));
			sleep(1000);
			
			phone.sendKeys(configTestRunner.getBaseAction().getTestData().get("phone"));
			configTestRunner.getChildTest().log(Status.INFO,"Entered Phone number as " + configTestRunner.getBaseAction().getTestData().get("phone"));
			sleep(1000);
			
			address.sendKeys(configTestRunner.getBaseAction().getTestData().get("address"));
			configTestRunner.getChildTest().log(Status.INFO,"Entered Address as " + configTestRunner.getBaseAction().getTestData().get("address"));
			sleep(1000);
			
			getStarted.click();
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on the getStarted Button");
			
			sleep(1000);
			try {
				if (fnWaitForVisibility(SuccessMsg,Constants.AJAX_TIMEOUT)) {
					configTestRunner.getChildTest().log(Status.PASS,"User Onboarding Successfull" + configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Onboarding Success")));
				} else
					configTestRunner.getChildTest().log(Status.FAIL,"User Onboarding failed" + configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Onboarding Failed")));

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				try {
					configTestRunner.getChildTest().log(Status.FAIL,
							"Something went wrong ,User Onboarding failed." + configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ErrorFailed")));
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}	
			
						
		}
	}
	
	
	public void fnverifySelectSMA(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Useronboarding Testcase for Selecting SMA and back Button to verify"));
		
		fnWaitForVisibility(bronzeville, 30);
		if(bronzeville.isDisplayed()) {
			String[] smas = configTestRunner.getBaseAction().getTestData().get("smaList").split(",");

			String[] family = configTestRunner.getBaseAction().getTestData().get("familyList").split(",");
			
			for (int i = 0; i < smas.length; i++) {
				sleep(1000);
				getElement("//div[contains(text(),'" + smas[i] + "')]").click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on " + smas[i] + " SMA ");
			}
			
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on the continue button after selecting sma");
			
			sleep(2000);
			backbtn.click();
			sleep(1000);
			try {
				configTestRunner.getChildTest().log(Status.PASS,"Taking Screenshot to check SMA selected" + configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ScreenshotSMA")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO, "Again Clicked on the continue button after taking screenshot for sma");
			
			for (int i = 0; i < family.length; i++) {
				sleep(1000);
				getElement("//div[contains(text(),'" + family[i] + "')]").click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on " + family[i] + " Family ");
			}

			sleep(1000);
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO,"Clicked on the continue button after selecting Families ");
			
			sleep(2000);
			scrollTillVisible(backbtn);
			executeExtJsClick(backbtn);
			sleep(1000);
			try {
				configTestRunner.getChildTest().log(Status.PASS,"Taking Screenshot to check families selected" + configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ScreenshotSMA")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void fnverifySelectFamily(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Useronboarding Testcase for Deselecting SMA and back Button to verify"));
		
		fnWaitForVisibility(bronzeville, 30);
		if(bronzeville.isDisplayed()) {
			String[] smas = configTestRunner.getBaseAction().getTestData().get("smaList").split(",");

			String[] family = configTestRunner.getBaseAction().getTestData().get("familyList").split(",");
			
			for (int i = 0; i < smas.length; i++) {
				sleep(1000);
				getElement("//div[contains(text(),'" + smas[i] + "')]").click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on " + smas[i] + " SMA ");
			}
			
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on the continue button after selecting sma");
			
			sleep(2000);
			scrollTillVisible(backbtn);
			executeExtJsClick(backbtn);
			sleep(1000);
			
			for (int i = 1; i < smas.length; i++) {
				sleep(1000);
				getElement("//div[contains(text(),'" + smas[i] + "')]").click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on " + smas[i] + " SMA ");
			}
			
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on the continue button after deselecting sma");
			
			sleep(2000);
			scrollTillVisible(backbtn);
			executeExtJsClick(backbtn);
			sleep(1000);
			
			try {
				configTestRunner.getChildTest().log(Status.PASS,"Taking Screenshot to check SMA selected" + configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ScreenshotSMA")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO, "Again Clicked on the continue button after taking screenshot for sma");
			
			for (int i = 0; i < family.length; i++) {
				sleep(1000);
				getElement("//div[contains(text(),'" + family[i] + "')]").click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on " + family[i] + " Family ");
			}

			sleep(1000);
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO,"Clicked on the continue button after selecting Families ");
			
			sleep(2000);
			scrollTillVisible(backbtn);
			executeExtJsClick(backbtn);
			sleep(1000);
			
			
			for (int i = 1; i < family.length; i++) {
				sleep(1000);
				getElement("//div[contains(text(),'" + family[i] + "')]").click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on " + family[i] + " Family ");
			}
			
			scrollTillVisible(continuebtn);
			executeExtJsClick(continuebtn);
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on the continue button after deselecting family");
			
			sleep(2000);
			scrollTillVisible(backbtn);
			executeExtJsClick(backbtn);
			sleep(1000);
			
			try {
				configTestRunner.getChildTest().log(Status.PASS,"Taking Screenshot to check family selected" + configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ScreenshotSMA")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	
	
}
