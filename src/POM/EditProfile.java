package POM;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import Application.BaseAction;
import Application.ConfigTestRunner;
import Utility.Constants;

public class EditProfile extends BaseAction {
	
	public EditProfile(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//div[contains(text(),'Your Profile ')]")
	WebElement profileLink;
	
	@FindBy(xpath = "//img[@alt='profile' and @class='user-profile-img']")
    WebElement profile;
	
	@FindBy(xpath = "//a[contains(text(),'Edit Profile')]")
	WebElement editBtn;
	
	@FindBy(xpath ="//input[@formcontrolname='firstName']")
	WebElement fname;
	
	@FindBy(xpath ="//input[@formcontrolname='lastName']")
	WebElement lname;
	
	@FindBy(xpath ="//input[@formcontrolname='userName']")
	WebElement prefName;
	
	@FindBy(xpath ="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath = "//input[@formcontrolname='address']")
	WebElement address;
	
	@FindBy(xpath = "//select[@id='gender']")
	WebElement gender;
	
	@FindBy(xpath = "//input[@id='birthday']")
	WebElement birthday;
	
	@FindBy(xpath = "//select[@id='countrycode']")
	WebElement countrycode;
	
	@FindBy(xpath = "//button[@class='save-btn' and contains(text(),'Save')]")
	WebElement saveBtn;
	
	
	@FindBy(xpath = "//button[@class='cancel-btn' and contains(text(),'Cancel')]")
	WebElement cancelBtn;
	
	
	@FindBy(xpath = "//div[contains(text(),'Profile updated successfully')]")
	WebElement toastNoti;
	
	public void fnEditProfile(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("To Edit profile of the User "));
		
		if(fnWaitForVisibility(profile, 20)) {
			profile.click();
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on Profile Icon");
			sleep(1000);
			
			profileLink.click();
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on your profile link");
			sleep(1000);
			
			if(fnWaitForVisibility(editBtn, 20)) {
				sleep(1000);
				editBtn.click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on the edit profile link");
				
				fname.clear();
				sleep(1000);
				fname.sendKeys(configTestRunner.getBaseAction().getTestData().get("fname"));
				configTestRunner.getChildTest().log(Status.INFO, "Inserted first name as "+configTestRunner.getBaseAction().getTestData().get("fname"));
				sleep(1000);
				
				lname.clear();
				sleep(1000);
				lname.sendKeys(configTestRunner.getBaseAction().getTestData().get("lname"));
				configTestRunner.getChildTest().log(Status.INFO, "Inserted last name as "+configTestRunner.getBaseAction().getTestData().get("lname"));
				sleep(1000);
				
				prefName.clear();
				sleep(1000);
				prefName.sendKeys(configTestRunner.getBaseAction().getTestData().get("prefname"));
				configTestRunner.getChildTest().log(Status.INFO, "Inserted preferred name as "+configTestRunner.getBaseAction().getTestData().get("prefname"));
				sleep(1000);
				
				getSelectWebElementPOM(gender).selectByVisibleText(configTestRunner.getBaseAction().getTestData().get("gender"));
				configTestRunner.getChildTest().log(Status.INFO, "Selected gender as "+configTestRunner.getBaseAction().getTestData().get("gender"));
				sleep(1000);
				
				birthday.sendKeys(configTestRunner.getBaseAction().getTestData().get("birthdate"));
				configTestRunner.getChildTest().log(Status.INFO, "Entered birthdate as "+configTestRunner.getBaseAction().getTestData().get("birthdate"));
				sleep(1000);
				
				getSelectWebElementPOM(countrycode).selectByVisibleText(configTestRunner.getBaseAction().getTestData().get("ccode"));
				configTestRunner.getChildTest().log(Status.INFO, "Selected country code as "+configTestRunner.getBaseAction().getTestData().get("ccode"));
				sleep(1000);
				
				phone.clear();
				sleep(1000);
				phone.sendKeys(configTestRunner.getBaseAction().getTestData().get("phone"));
				configTestRunner.getChildTest().log(Status.INFO, "Entered phone number as "+configTestRunner.getBaseAction().getTestData().get("phone"));
				sleep(1000);
				
				address.clear();
				sleep(1000);
				address.sendKeys(configTestRunner.getBaseAction().getTestData().get("address"));
				configTestRunner.getChildTest().log(Status.INFO, "Entered address numer as "+configTestRunner.getBaseAction().getTestData().get("address"));
				sleep(1000);
				
				scrollTillVisible(saveBtn);
				executeExtJsClick(saveBtn);
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on save button");
				sleep(500);
				
				try{
	                if (fnWaitForVisibility(toastNoti, Constants.AJAX_TIMEOUT) ) {
	                    configTestRunner.getChildTest().log(Status.PASS,"Profile Updated Successfully"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ProfileUpdated")));
	                }else {
	                	configTestRunner.getChildTest().log(Status.FAIL, "Profile not updated"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ProfileNotUpdated")));
	                }
	            }catch (Exception e){
	                e.printStackTrace();
	                try{
	                    configTestRunner.getChildTest().log(Status.FAIL, "Profile not updated something went wrong"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ProfileNotUpdatedException")));
	                }catch (Exception e1){

	                }
	            }
				
				sleep(4000);
				profile.click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on Profile Icon");
				sleep(1000);
				
				profileLink.click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on your profile link");
				sleep(1000);
				
				try {
					configTestRunner.getChildTest().log(Status.PASS,"Updated Profile Screenshot for verification"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ProfileUpdated")));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			
			
		}
		
	}
	
	
}
