package POM;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import Application.BaseAction;
import Application.ConfigTestRunner;
import Utility.Constants;

public class SuUserRegistration extends BaseAction {

	public SuUserRegistration(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "//img[@alt='profile' and @class='user-profile-img']")
    WebElement profile;
	
	
	@FindBy(xpath = "//div[contains(text(),'Admin Account ')]")
    WebElement adminac; 
	
	@FindBy(xpath="//span[contains(text(),'Users & Roles')]")
	WebElement useroles;
	
	@FindBy(xpath="//input[@formcontrolname='firstName']")
	WebElement fname;

	@FindBy(xpath="//input[@formcontrolname='lastName']")
	WebElement lname;
	
	@FindBy(xpath="//input[@id='email']")
	WebElement email;
	
	@FindBy(xpath="//select[@id='gender']")
	WebElement gender;
	
	@FindBy(xpath="//input[@id='birthday']")
	WebElement birthday;
	
	@FindBy(xpath="//select[@id='countrycode']")
	WebElement ccode;
	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//input[@formcontrolname='address']")
	WebElement address;
	
	@FindBy(xpath="//select[@id='subscription']")
	WebElement subscription;
	
	@FindBy(xpath="//select[@id='role']")
	WebElement role;
	
	@FindBy(xpath="//input[@formcontrolname='photourl']")
	WebElement photo;
	
	@FindBy(xpath="(//div[@role='combobox']/input)[1]")
	WebElement sma;

	@FindBy(xpath="(//div[@role='combobox']/input)[2]")
	WebElement family;

	
	@FindBy(xpath="//button[@class= 'btn btn-save' and contains(text(),'Save')]")
	WebElement savebtn;
	
	@FindBy(xpath="//button[ contains(text(),'Save & Add New')]")
	WebElement saveAddbtn;
	
	@FindBy(xpath="//div[contains(text(),'New user created successfully. ')]/button[@class='btn-close']")
	WebElement closeNoti;
	
	public void AddUserBySuperAdmin(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Registering user thought Admin Account"));
		
		if(fnWaitForVisibility(profile, 20)) {
			
			profile.click();
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on Profile Icon");
			sleep(1000);
			
			adminac.click();
			configTestRunner.getChildTest().log(Status.INFO, "Clicked on admin account");
			sleep(1000);
			
			if(fnWaitForVisibility(useroles, 15)) {
				
				useroles.click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on User Roles Option");
				sleep(1000);
				
			}else {
				configTestRunner.getChildTest().log(Status.INFO, "User option is not visible");
			}
			
			fname.sendKeys(configTestRunner.getBaseAction().getTestData().get("fname"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered First Name as "+configTestRunner.getBaseAction().getTestData().get("fname"));
			sleep(1000);
			
			lname.sendKeys(configTestRunner.getBaseAction().getTestData().get("lname"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered Last Name as "+configTestRunner.getBaseAction().getTestData().get("lname"));
			sleep(1000);
			
			email.sendKeys(configTestRunner.getBaseAction().getTestData().get("email"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered Email address as "+configTestRunner.getBaseAction().getTestData().get("email"));
			sleep(1000);
			
			
			
			getSelectWebElementPOM(gender).selectByVisibleText(configTestRunner.getBaseAction().getTestData().get("gender"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered gender as "+configTestRunner.getBaseAction().getTestData().get("gender"));
			sleep(1000);
			
			birthday.sendKeys(configTestRunner.getBaseAction().getTestData().get("birthdate"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered Birthdate as "+configTestRunner.getBaseAction().getTestData().get("birthdate"));
			sleep(1000);
			
			getSelectWebElementPOM(ccode).selectByVisibleText(configTestRunner.getBaseAction().getTestData().get("ccode"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered Country code as "+configTestRunner.getBaseAction().getTestData().get("ccode"));
			sleep(1000);
			
			phone.sendKeys(configTestRunner.getBaseAction().getTestData().get("phone"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered Phone Number as "+configTestRunner.getBaseAction().getTestData().get("phone"));
			sleep(1000);
			
			address.sendKeys(configTestRunner.getBaseAction().getTestData().get("address"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered Address as "+configTestRunner.getBaseAction().getTestData().get("address"));
			sleep(1000);
			
			getSelectWebElementPOM(subscription).selectByVisibleText(configTestRunner.getBaseAction().getTestData().get("sub"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered Subscription as "+configTestRunner.getBaseAction().getTestData().get("sub"));
			sleep(1000);
			
			getSelectWebElementPOM(role).selectByVisibleText(configTestRunner.getBaseAction().getTestData().get("role"));
			configTestRunner.getChildTest().log(Status.INFO, "Entered Role as "+configTestRunner.getBaseAction().getTestData().get("role"));
			sleep(1000);
			
			
			
//			String[] smas = configTestRunner.getBaseAction().getTestData().get("smas").split(",");
//			
//			scrollTillVisible(sma);
//			executeExtJsClick(sma);
//			configTestRunner.getChildTest().log(Status.INFO, "Clicked on SMA dropdown");
//			
//			for(int i=0; i<smas.length ;i++) {
//				sleep(1500);
//				scrollTillVisible(getElement("//div/span[@class='ng-option-label' and contains(text(),'"+smas[i]+"')]"));
//				getElement("//div/span[@class='ng-option-label' and contains(text(),'"+smas[i]+"')]").click();
//				configTestRunner.getChildTest().log(Status.INFO, "Selected SMA is "+smas[i]);
//				sleep(1000);
//			}
//			
//			scrollTillVisible(family);
//			executeExtJsClick(family);
//			
//			configTestRunner.getChildTest().log(Status.INFO, "Clicked on family dropdown");
//			sleep(1000);
//			
//			String[] families = configTestRunner.getBaseAction().getTestData().get("families").split(",");
//			
//			for(int i=0; i<smas.length ;i++) {
//				sleep(1500);
//				scrollTillVisible(getElement("//div/span[@class='ng-option-label' and contains(text(),'"+families[i]+"')]"));
//				getElement("//div/span[@class='ng-option-label' and contains(text(),'"+families[i]+"')]").click();
//				configTestRunner.getChildTest().log(Status.INFO, "Selected Family is "+families[i]);
//				sleep(1000);
//			}
			
			
//			photo.sendKeys(configTestRunner.getBaseAction().getTestData().get("image"));
//			configTestRunner.getChildTest().log(Status.INFO, "Selected Image path is "+configTestRunner.getBaseAction().getTestData().get("image"));
//			sleep(1000);
			scrollTillVisible(savebtn);
			if(configTestRunner.getBaseAction().getTestData().get("saveButton").equalsIgnoreCase("Yes")) {
				if( savebtn.isEnabled()) {
					executeExtJsClick(savebtn);
					configTestRunner.getChildTest().log(Status.INFO, "Clicked on the Save Button");
				}else {
					configTestRunner.getChildTest().log(Status.ERROR, "Button is not Enabled User can not be created");
				}
			}else {
				if( saveAddbtn.isEnabled()) {
					executeExtJsClick(saveAddbtn);
					configTestRunner.getChildTest().log(Status.INFO, "Clicked on the Save Button");
				}else {
					configTestRunner.getChildTest().log(Status.ERROR, "Button is not Enabled User can not be created");
				}
			}
			
			sleep(1000);
			
			
			 try{
	                if (fnWaitForVisibility(closeNoti, Constants.AJAX_TIMEOUT) ) {
	                    configTestRunner.getChildTest().log(Status.PASS,"User Created Successfully"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("UserCreated")));
	                } else
	                    configTestRunner.getChildTest().log(Status.FAIL, "User Not Created No message Displayed"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("UserNotCreated")));
	            }catch (Exception e){
	                e.printStackTrace();
	                try{
	                    configTestRunner.getChildTest().log(Status.FAIL, "User Not Created something went wrong"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("UserNotCreatedException")));
	                }catch (Exception e1){

	                }
	            }
			
			
		}		
		
	}	
	
}
