package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import Application.BaseAction;
import Application.ConfigTestRunner;
import Utility.Constants;
// now we are extending baseAction class which has all the reusable methods which we can use here.
public class Registration extends BaseAction {
	
	// we are using the constructor to initialize the webelements created using POM factory method.
	public Registration(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	// using @findby we can create WebElements with there xpath, and furthur we can perform selenium actions on them
	@FindBy(xpath="//a[contains(text(),'Register')]")
	WebElement reglink;
	
	@FindBy(xpath="//input[@id='email']")
	WebElement email;
	
	@FindBy(xpath="//input[@formcontrolname='password']")
	WebElement password;
	
	@FindBy(xpath="//input[@formcontrolname='confirmPassword']")
	WebElement confirm;
	
	@FindBy(xpath="//button[contains(text(),'Register')]")
	WebElement contBtn;
	
	@FindBy(xpath="//div[contains(text(),'Email sent to registered email id successfully')]")
	WebElement SuccessMsg;
	
	
	public void FnRegistration(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Registration of User"));
		if(fnWaitForVisibility(reglink,Constants.AJAX_TIMEOUT)) {
			
			scrollTillVisible(reglink);
			sleep(1000);
			executeExtJsClick(reglink);
			configTestRunner.getChildTest().log(Status.INFO, "Clicked Registation Link");
			sleep(1000);
			
			if(fnWaitForVisibility(contBtn, Constants.AJAX_TIMEOUT)) {
				
				
				email.sendKeys(configTestRunner.getBaseAction().getTestData().get("email"));
				configTestRunner.getChildTest().log(Status.INFO, "Entered Email as "+configTestRunner.getBaseAction().getTestData().get("email"));
				sleep(1000);
				password.sendKeys(configTestRunner.getBaseAction().getTestData().get("password"));
				configTestRunner.getChildTest().log(Status.INFO, "Entered Password as "+configTestRunner.getBaseAction().getTestData().get("password"));
				sleep(1000);
				confirm.sendKeys(configTestRunner.getBaseAction().getTestData().get("confirmPass"));
				configTestRunner.getChildTest().log(Status.INFO, "Entered Confirm Password as "+configTestRunner.getBaseAction().getTestData().get("confirmPass"));
				sleep(1000);
		
		
				sleep(1000);
				contBtn.click();
				configTestRunner.getChildTest().log(Status.INFO, "Clicked on Registration Button");
				sleep(3000);
				
				
				try {
					if(fnWaitForVisibility(SuccessMsg,Constants.AJAX_TIMEOUT)) {
						configTestRunner.getChildTest().log(Status.PASS, "User Registration Successfull"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Registration Success")));
					}else 
						configTestRunner.getChildTest().log(Status.FAIL, "User Registration failed"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Registration Failed")));
										
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					try {
	                    configTestRunner.getChildTest().log(Status.FAIL, "Something went wrong ,User Registration failed."+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("ErrorFailed")));
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
				
			}
			
			
		}
	}
	
}
