package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import com.aventstack.extentreports.Status;

import Application.BaseAction;
import Application.ConfigTestRunner;
import Utility.Constants;

public class InviteUsers extends BaseAction {
	public InviteUsers(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//button[contains(text(),' Invite a friend and earn more')]")
	WebElement invitelink;
	
	@FindBy(xpath="//input[@id='email']")
	WebElement inputEmail;
	
	@FindBy(xpath="//button[contains(text(),'Add')]")
	WebElement addBtn;
	
	@FindBy(xpath="//button[contains(text(),'Send invite')]")
	WebElement sendBtn;  
	
	@FindBy(xpath="//button[@class='close']")
	WebElement closeBtn; 
	
	@FindBy(xpath="//div[contains(text(),'Invitation link sent')]")
	WebElement successNote;
	
	// closing the email xpath //span[contains(text(),'test1@gmail.com')]//span
	
	public void fnInviteUsers(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Invite Users to Join Bronzeverse"));
		scrollTillVisible(invitelink);
        fnWaitForVisibility(invitelink, 30);
        if(invitelink.isDisplayed()) {
        	invitelink.click();
        	configTestRunner.getChildTest().log(Status.INFO, "Clicked on the invitelink");
        	
        	String[] emails = configTestRunner.getBaseAction().getTestData().get("email").split(",");
        	
        	for(int i = 0; i < emails.length ; i++) {
        		inputEmail.sendKeys(emails[i]);
            	configTestRunner.getChildTest().log(Status.INFO, "Entered email address as "+emails[i]);
            	
            	addBtn.click();
            	configTestRunner.getChildTest().log(Status.INFO, "Clicked on the Add Button");
            	
        		
        	}
        	
        	sleep(1500);
        	
        	sendBtn.click();
        	configTestRunner.getChildTest().log(Status.INFO, "Clicked on the Send Button");
        	
        	sleep(2000);
        	try{
                if (fnWaitForVisibility(successNote,  Constants.AJAX_TIMEOUT)) {
                    configTestRunner.getChildTest().log(Status.PASS,"Invite link to User sent successful"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Invite_Sent")));
                } else
                    configTestRunner.getChildTest().log(Status.FAIL, "Invite link to User not sent"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Invite_NotSent")));
            }catch (Exception e){
                e.printStackTrace();
                try{
                    configTestRunner.getChildTest().log(Status.FAIL, "Invite link to User not sent"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.capture("Invite_NotSent")));
                }catch (Exception e1){

                }
            }
        	
        	closeBtn.click();
        	configTestRunner.getChildTest().log(Status.INFO, "Clicked on the Close window button");
        	
        	
        }
	}
	
	
}
