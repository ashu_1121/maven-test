package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import Application.BaseAction;
import Application.ConfigTestRunner;
import Utility.Constants;

public class ToggleBtn extends BaseAction {

	public ToggleBtn(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span/small[contains(text(),'SHOW')]")
	WebElement showbtn;
	
	@FindBy(xpath="//span/small[contains(text(),'HIDE')]")
	WebElement hidebtn;
	
	@FindBy(xpath="//input[@id='password']")
	WebElement pass;
	
	
	public void fnVerifyToggle(ConfigTestRunner configTestRunner) {
		configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Verify the toggle button"));
        configTestRunner.getChildTest().log(Status.INFO, "URL used for the application is "+ Constants.URL);
        
        if(fnWaitForVisibility(showbtn, 10)) {
        	sleep(1000);
        	pass.sendKeys(configTestRunner.getBaseAction().getTestData().get("password"));
        	configTestRunner.getChildTest().log(Status.INFO, "Entered password as "+configTestRunner.getBaseAction().getTestData().get("password"));
        	sleep(1000);
        	
        	showbtn.click();
        	configTestRunner.getChildTest().log(Status.INFO, "Clicked on the show button");
        	sleep(1000);
        	try {
        		configTestRunner.getChildTest().log(Status.INFO,"Taking the screenshot for show button "+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("showBtnSS")));
			} catch (Exception e) {
				// TODO: handle exception
			}
        	sleep(1000);
        	if(fnWaitForVisibility(hidebtn , 10)){
        		hidebtn.click();
        	
        	configTestRunner.getChildTest().log(Status.INFO, "Clicked on the Hide button");
        	sleep(1000);
	        	try {
	        		configTestRunner.getChildTest().log(Status.INFO,"Taking the screenshot for hide button "+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("hideBtnSS")));
				} catch (Exception e) {
					// TODO: handle exception
				}
        	}
        	
        }
        
        
	}
	
	
}
