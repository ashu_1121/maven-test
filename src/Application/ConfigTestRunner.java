package Application;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import Config.Configuration;
import POM.BrokenLink;
import POM.CreateAnnouncement;
import POM.EditProfile;
import POM.InviteUsers;
import POM.Registration;
import POM.SuUserRegistration;
import POM.ToggleBtn;
import POM.UserLogin;
import POM.UserOnboardingPom;
import Utility.ElementUtil;

public class ConfigTestRunner {

	private String destFile;
	private ConfigTestRunner configTestRunner = null;
	private com.aventstack.extentreports.ExtentTest parentTest;
	private com.aventstack.extentreports.ExtentTest childTest;
	private ExtentReports extent;
	private BaseAction baseAction;
	private Configuration config;
	public ElementUtil elementUtil;
	// mention the action classes and create one object which will be used in SCExectuor to call action methods
	public Loginxpath loginxpath;
	public UserLogin userLogin;
	public InviteUsers inviteUser;
	public SuUserRegistration suUserRegistration;
	public Registration registrtion;
	public UserOnboardingPom userOnboardingPom;
	public CreateAnnouncement createAnnouncement;
	public EditProfile editprofile;
	public BrokenLink brokenLink;
	public ToggleBtn toggle;
	public WebDriver driver = DriverFactory.getDriver();
	public String TestCase_Id;
	public Map<String, String> testCase = new ConcurrentHashMap<>();
	public Map<String, String> testData = new ConcurrentHashMap<>();
	

	public ConfigTestRunner(ExtentReports extent) {
		setExtent(extent);
	}

	public void run(String Tcnumber) {
		baseAction = new BaseAction();
		baseAction.ReadTestData(Tcnumber);
		//System.out.println("Read test data run method");
		// TC number passed from @test will be compared with excelsheet data, if execute column has Yes written in it
		// it will execute the testcase otherwise it will skip.
		if (baseAction.testCase.get("Execute").equalsIgnoreCase("YES")) {
			parentTest = extent.createTest(baseAction.testCase.get("TC_ID") + " " + baseAction.testCase.get("Description"));
			
			intActions();
			TestCase_Id = baseAction.testCase.get("TC_ID");
			
			int rowNo = baseAction.RowNo("TestData", Tcnumber, "TC_ID");
			baseAction.TestDataDic(rowNo, "TestData");
			// login methods to write here
			driver.get(baseAction.testCase.get("url"));
			//when user registration is there, it do not requires login, so we skip login with 
			if(baseAction.testCase.get("LoginRequired").equalsIgnoreCase("YES")) {
				userLogin.loginToApplication(configTestRunner);
				SCExecutor(Tcnumber);
				userLogin.LogOutFromApplication(configTestRunner);
			}else {			
				SCExecutor(Tcnumber);
			}
			// driver.close();
		} else
			parentTest.log(Status.INFO, "No Test Case is considered for execution");

	}
	
	// to identify which class file instance method to call as per the testcase number.
	public void SCExecutor(String tcNumber) {
		if(tcNumber.equalsIgnoreCase("TC002") || tcNumber.equalsIgnoreCase("TC002D") || tcNumber.equalsIgnoreCase("TC0021") ||  tcNumber.equalsIgnoreCase("TC0022") || tcNumber.equalsIgnoreCase("TC0023") || tcNumber.equalsIgnoreCase("TC0024") || tcNumber.equalsIgnoreCase("TC0025") || tcNumber.equalsIgnoreCase("TC0026") || tcNumber.equalsIgnoreCase("TC0027") || tcNumber.equalsIgnoreCase("TC0028") || tcNumber.equalsIgnoreCase("TC0029") || tcNumber.equalsIgnoreCase("TC00291") || tcNumber.equalsIgnoreCase("TC00292") || tcNumber.equalsIgnoreCase("TC00293") || tcNumber.equalsIgnoreCase("TC00294") || tcNumber.equalsIgnoreCase("TC00295") || tcNumber.equalsIgnoreCase("TC00296") || tcNumber.equalsIgnoreCase("TC00297") || tcNumber.equalsIgnoreCase("TC00298") || tcNumber.equalsIgnoreCase("TC00299") ) {
			// to call specfic function we need to first create instance of the action class at the top then import the class, now initialise the instance
			// then call the action method with instance using ( . ) dot operator and pass the configtestrunner object
			registrtion.FnRegistration(configTestRunner);
		}else if(tcNumber.equalsIgnoreCase("TC004") || tcNumber.equalsIgnoreCase("TC004D") || tcNumber.equalsIgnoreCase("TC041") || tcNumber.equalsIgnoreCase("TC042") || tcNumber.equalsIgnoreCase("TC043") || tcNumber.equalsIgnoreCase("TC044") || tcNumber.equalsIgnoreCase("TC045") ) {
			inviteUser.fnInviteUsers(configTestRunner);
		}else if(tcNumber.equalsIgnoreCase("TC005") || tcNumber.equalsIgnoreCase("TC005D") || tcNumber.equalsIgnoreCase("TC501") || tcNumber.equalsIgnoreCase("TC502") || tcNumber.equalsIgnoreCase("TC503") || tcNumber.equalsIgnoreCase("TC504") || tcNumber.equalsIgnoreCase("TC505") || tcNumber.equalsIgnoreCase("TC506") || tcNumber.equalsIgnoreCase("TC507") || tcNumber.equalsIgnoreCase("TC508") || tcNumber.equalsIgnoreCase("TC051") || tcNumber.equalsIgnoreCase("TC052") || tcNumber.equalsIgnoreCase("TC053") || tcNumber.equalsIgnoreCase("TC054") || tcNumber.equalsIgnoreCase("TC055") || tcNumber.equalsIgnoreCase("TC0051") || tcNumber.equalsIgnoreCase("TC0052") || tcNumber.equalsIgnoreCase("TC0053") || tcNumber.equalsIgnoreCase("TC0054") || tcNumber.equalsIgnoreCase("TC0055") || tcNumber.equalsIgnoreCase("TC0056") || tcNumber.equalsIgnoreCase("TC0057") || tcNumber.equalsIgnoreCase("TC0058") || tcNumber.equalsIgnoreCase("TC0059") ) {
			suUserRegistration.AddUserBySuperAdmin(configTestRunner);
		}else if(tcNumber.equalsIgnoreCase("TC006") || tcNumber.equalsIgnoreCase("TC006D")) {
			userOnboardingPom.fnOnboarding(configTestRunner);
		}else if(tcNumber.equalsIgnoreCase("TC007") || tcNumber.equalsIgnoreCase("TC007D")) {
			createAnnouncement.FnCreateAnnouncement(configTestRunner);
		}else if(tcNumber.equalsIgnoreCase("TC008")) {
			editprofile.fnEditProfile(configTestRunner);
		}else if(tcNumber.equalsIgnoreCase("TC009")) {
			brokenLink.fnBrokenLinks(configTestRunner);
		}else if(tcNumber.equalsIgnoreCase("TCtoggle1")) {
			toggle.fnVerifyToggle(configTestRunner);
		}
	}

	// to initialize the action classes which will be called in SCExecutor
	public void intActions() {
		elementUtil = new ElementUtil(driver);
		config = new Configuration();
		loginxpath = new Loginxpath();
		// we pass driver to the constructor of the class to initialize the webelements.
		registrtion = new Registration(driver);
		userLogin = new UserLogin(driver);
		inviteUser = new InviteUsers(driver);
		suUserRegistration = new SuUserRegistration(driver);
		userOnboardingPom = new UserOnboardingPom(driver);
		createAnnouncement = new CreateAnnouncement(driver);
		editprofile = new EditProfile(driver);
		brokenLink = new BrokenLink();
		toggle = new ToggleBtn(driver);
	}

	public String screenShotName(String screenShotName) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH-mm-ss");
		String fileName = screenShotName + "_" + dateFormat.format(new Date()) + ".png";
		return capture(fileName);
	}

	public String capture(String screenShotName) {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		System.out.println(getDestFile());
		String dest = getDestFile() + "\\" + screenShotName;
		File destination = new File(dest);
		try {
			FileUtils.copyFile(source, destination);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dest;
	}

	public Map<String, String> getTestData() {
		return testData;
	}

	public void setTestData(Map<String, String> testData) {
		this.testData = testData;
	}

	public ExtentReports getExtent() {
		return extent;
	}

	public void setExtent(ExtentReports extent) {
		this.extent = extent;
	}

	public void setConfigTestRunner(ConfigTestRunner configTestRunner) {
		this.configTestRunner = configTestRunner;
	}

	public ConfigTestRunner getConfigTestRunner() {
		return configTestRunner;
	}

	public String getDestFile() {
		return destFile;
	}

	public void setDestFile(String destFile) {
		this.destFile = destFile;
	}

	public ExtentTest getParentTest() {
		return parentTest;
	}

	public void setParentTest(ExtentTest parentTest) {
		this.parentTest = parentTest;
	}

	public ExtentTest getChildTest() {
		return childTest;
	}

	public void setChildTest(ExtentTest childTest) {
		this.childTest = childTest;
	}

	public Map<String, String> getTestCase() {
		return testCase;
	}

	public BaseAction getBaseAction() {
		return baseAction;
	}

	public void setBaseAction(BaseAction baseAction) {
		this.baseAction = baseAction;
	}

	public Configuration getConfig() {
		return config;
	}

	public void setConfig(Configuration config) {
		this.config = config;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
}
