package Application;

import Utility.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import java.util.concurrent.TimeUnit;

public class DriverFactory {

	public static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();
	public String driverpath;

	/*******
	 * This method is used to initialized the browser
	 *
	 */
	public WebDriver startBrowser(String browser) {
		if (browser.trim().equalsIgnoreCase("Chrome")) {
			driverpath = Constants.driverPath + "/chromedriver.exe";
			System.setProperty("webdriver.chrome.driver", driverpath);
			tlDriver.set(new ChromeDriver());
			setImplicitlyWait(Constants.Implicit_Wait);

		}else if (browser.trim().equalsIgnoreCase("firefox")) {
			driverpath = Constants.driverPath + "/geckodriver.exe";
			System.setProperty("webdriver.gecko.driver", driverpath);
			tlDriver.set(new FirefoxDriver());
			setImplicitlyWait(Constants.Implicit_Wait);
		} else {
			System.out.println("No Browser is selected for opening");
		}
		getDriver().manage().window().maximize();
		getDriver().manage().deleteAllCookies();
		return getDriver();
	}

	/*
	 * This method is used to apply the Implicit wait on the browser
	 */
	public void setImplicitlyWait(int seconds) {
		getDriver().manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}
	
	public static synchronized WebDriver getDriver() {
		return tlDriver.get();
	}

}
