
package Application;



import Utility.Constants;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebElement;

public class Loginxpath extends BaseAction {
    private String formName="login";

    public boolean loginToApplication(ConfigTestRunner configTestRunner) {
        configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("Login To BronzeVerse"));
        configTestRunner.getChildTest().log(Status.INFO, "URL use to login into the application is "+ Constants.URL);
        boolean isPresence = false;
        WebElement element = getWebElement("lemail",formName,configTestRunner);
        configTestRunner.elementUtil.fnWaitForVisibility(element,Constants.AJAX_TIMEOUT);
        sleep(1000);
        if(getWebElement("loginbtn",formName,configTestRunner).isDisplayed()) {
            getWebElement("lemail", formName, configTestRunner).sendKeys(configTestRunner.getBaseAction().getTestCase().get("UserName"));
            configTestRunner.getChildTest().log(Status.INFO, "Entered UserName as"+configTestRunner.getBaseAction().getTestCase().get("UserName"));
            sleep(1000);
            getWebElement("lpassword", formName, configTestRunner).sendKeys(configTestRunner.getBaseAction().getTestCase().get("Password"));
            configTestRunner.getChildTest().log(Status.INFO, "Entered Password is "+configTestRunner.getBaseAction().getTestCase().get("Password"));
            sleep(1000);
            executeExtJsClick(getWebElement("loginbtn", formName, configTestRunner));
            //.click();
            configTestRunner.getChildTest().log(Status.INFO, "Clicked on Login Button");
            sleep(5000);
            try{
                if (fnWaitForVisibility(getWebElement("inviteLink",formName,configTestRunner), Constants.AJAX_TIMEOUT)) {
                    configTestRunner.getChildTest().log(Status.PASS,"Login to the application is successful"+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Login_Successfull")));
                } else
                    configTestRunner.getChildTest().log(Status.FAIL, "Login to the application is successful."+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.screenShotName("Login_UnSuccessfull")));
            }catch (Exception e){
                e.printStackTrace();
                try{
                    configTestRunner.getChildTest().log(Status.FAIL, "Login to the application is not successful."+configTestRunner.getChildTest().addScreenCaptureFromPath(configTestRunner.capture("Login_UnSuccessfull")));
                }catch (Exception e1){

                }
            }
        }
        return isPresence;
    }
    public synchronized void LogOutFromApplication(ConfigTestRunner configTestRunner) {
    	sleep(5000);
        configTestRunner.setChildTest(configTestRunner.getParentTest().createNode("LogOut From Application Verification"));
//        configTestRunner.elementUtil.fnWaitForVisibility(getWebElement("lgout",formName,configTestRunner),Constants.AJAX_TIMEOUT);
//        configTestRunner.elementUtil.waitAndClick(getWebElement("lgout",formName,configTestRunner),Constants.AJAX_TIMEOUT);
//        if(configTestRunner.elementUtil.fnWaitForVisibility(getWebElement("userName",formName,configTestRunner),Constants.AJAX_TIMEOUT))
            configTestRunner.getChildTest().log(Status.PASS, "User is able to log out from the application");
//        else
//            configTestRunner.getChildTest().log(Status.FAIL, "User is not able to log out from the application");
    }

}


