package Application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.security.auth.login.Configuration;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import Utility.Constants;

public class BaseAction {
	public WebDriver driver = DriverFactory.getDriver() ;
	protected Configuration conf;
	public String driverpath;
	public String destFile;
	public ExtentTest test;
	private ExtentReports extent;
	private static XSSFWorkbook wb;
	private static XSSFSheet sh;
	private int Rowcnt;
	File Src;
	public Map<String, String> testCase = new ConcurrentHashMap<String, String>();
	private Map<String, String> testData = new ConcurrentHashMap<String, String>();
	private boolean Exist = false;
	public com.aventstack.extentreports.ExtentTest parentTest;
	public com.aventstack.extentreports.ExtentTest childTest;

	// to access the driver instance as local variable , it will be initialised in
	// driverfactory class
	public WebDriver getDriver() {
		return driver;
	}

		
	public void selectComboBox(WebElement element, String value) {
		waitAndClick(element, Constants.AJAX_TIMEOUT, driver);
		waitAndClick(driver.findElement(By.xpath("//li[text()='"+value+"']")), Constants.AJAX_TIMEOUT, driver);
	}
	
	public String getCurrentURL() {
		return driver.getCurrentUrl();
	}
	
	public void selectComboBoxContains(WebElement element, String value) {
		waitAndClick(element, Constants.AJAX_TIMEOUT, driver);
		waitAndClick(driver.findElement(By.xpath("//li[contains(text(),'"+value+"')]")), Constants.AJAX_TIMEOUT, driver);
	}
	
	public void handleAlertAccept() {
		Alert promptAlert = driver.switchTo().alert();
		promptAlert.accept();
		
	}
	
	public String readAlertText() {
		Alert promptAlert = driver.switchTo().alert();
		return promptAlert.getText();
	}
	
	public String waitForAlert() {
		WebDriverWait waitAlert = new WebDriverWait(driver, 20);
		String alertText;
		if(waitAlert.until(ExpectedConditions.alertIsPresent())!= null) {
			alertText = readAlertText();
			handleAlertAccept();
			return alertText;
		}
		return null;
	}

	// scroll till the particular element is visible
	public void scrollTillVisible(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	// for horizontal , if it is division.
	public void scrollTillVisibleHorizontal(WebElement element) {
		((JavascriptExecutor) driver).executeScript("argument[0].scrollIntoView();", element);
	}

	// wait till particular condition is complete
	public void waitToClick(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element)).click();

	}

	// refresh the page
	public void pageRefresh() {
		driver.navigate().refresh();
	}
	
	public void goToUrl(String url) {
		driver.get(url);
	}
	
	// wait for certain seconds then click on the element
	public void waitAndClick(final WebElement element, int waitfor) {
        WebDriverWait wait = new WebDriverWait(driver, waitfor);
        wait.until(new ExpectedCondition<WebElement>() {
            public final ExpectedCondition<WebElement> visibilityOfElement = ExpectedConditions.visibilityOf(element);
            @Override
            public WebElement apply(WebDriver driver) {
                try {
                    WebElement elementx = this.visibilityOfElement.apply(driver);
                    if (elementx == null) {
                        return null;
                    }
                    if (elementx.isDisplayed() && elementx.isEnabled())  {
                        elementx.click();
                        return elementx;
                    } else {
                        return null;
                    }
                } catch (WebDriverException e) {
                    return null;
                }
            }
        });
    }
	
	// to read excel data it will stored in wb workbook
	public void readExcelDataFile(String FilePath) {
		InputStream is;
		FileInputStream Src = null;
		System.out.println( Constants.TestDataPath + FilePath);
		if (!Constants.TestDataPath.isEmpty()) {
			try {
				Src = new FileInputStream(Constants.TestDataPath + FilePath);
				System.out.println(Src);
			} catch (Exception e) {

			}

		} else {
			is = getClass().getResourceAsStream(FilePath);
			try {
				wb = new XSSFWorkbook(is);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Path of the File is" + is);
		}
		try {
			wb = new XSSFWorkbook(Src);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

		
	public void selectTextBox(String xpath) {
		driver.findElement(By.xpath(xpath));
	} 
	
	// with this method we will initialise and store actual data in wb
	public Map<String, String> ReadTestData(String TCID) {
		//System.out.println("Read data excel");
		readExcelDataFile("TestData.xlsx");
		// int RCount = RowCount("TestCase");
		// int CCount = ColCount("TestCase");
		int rowno = RowNo("TestCase", TCID, "TC_ID");
		System.out.println("Read data excel");
		return TestCaseDic(rowno, "TestCase");
	}

	// for javascript related click.
	public void executeExtJsClick(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }

	public int RowCount(String SheetName) {
		sh = wb.getSheet(SheetName);
		return sh.getLastRowNum();
	}

	public int ColCount(String SheetName) {
		sh = wb.getSheet(SheetName);
		return sh.getRow(0).getLastCellNum();

	}

	// information from the excelsheet for particular testcase, converted into dictionary/json like format
	public Map<String, String> TestCaseDic(int m, String sheetName) {
		int CCnt = ColCount(sheetName);
		for (int q = 0; q < CCnt; q++) {
			String Key = getExcelDataint(sheetName, 0, q).trim();

			if (Key.equals("")) {
				testCase.put(Key, " ");
			} else {
				try {
					String Value = getExcelDataint(sheetName, m, q).trim();
					testCase.put(Key, Value);
				} catch (Exception e) {

				}
			}
		}
		return testCase;
	}

	// information from the excelsheet for particular testcase testdata, converted into dictionary/json like format
	public void TestDataDic(int RowNo, String sheetName) {
		int Ccnt = ColCount(sheetName);
		try {
			for (int j = 0; j < Ccnt; j++) {
				String key = getExcelDataint(sheetName, 0, j).trim();
				if (key.equals("")) {
					testData.put(key, null);
				} else {
					try {
						String value = getExcelDataint(sheetName, RowNo, j).trim();
						//System.out.println("key "+key+" Value "+value);
						testData.put(key, value);
					} catch (Exception ex) {
//                        ex.printStackTrace();
					}
				}
			}
		} catch (Exception e) {

		}
	}

	String getExcelDataint(String sheetName, int RowNo, int ColNo) {
		String CellValue = null;
		try {
			// int ColNo = ColumnValue(colNo);
			sh = wb.getSheet(sheetName);
			// Cell cell=sh.getRow(RowNo).getCell(ColNo);
			CellValue = sh.getRow(RowNo).getCell(ColNo).getStringCellValue();
		} catch (Exception ex) {
//             ex.printStackTrace();
		}
		return CellValue;
	}

	public int RowNo(String Sheet, String RowValue, String ColValue1) {
		int RCount = RowCount(Sheet);
		int Cocnt = ColumnValue(ColValue1, Sheet);
		for (int p = 1; p <= RCount; p++) {
			String Value = sh.getRow(p).getCell(Cocnt).getStringCellValue().trim();
			if (RowValue.trim().equals(Value.trim())) {
				Exist = true;
				return p;
			}
		}
		if (!Exist) {
			System.out.println(RowValue + " This row value is not exist.");
		}
		return -1;
	}

	public int rowValue(String RowData, int colNo) {
		Rowcnt = sh.getLastRowNum();
		for (int j = 0; j <= Rowcnt; j++) {
			try {
				String Value = sh.getRow(j).getCell(colNo).getStringCellValue().trim();
				if (RowData.trim().equals(Value.trim())) {
					Exist = true;
					return j;
				}
			} catch (Exception e1) {

			}
		}
		if (!Exist) {
			System.out.println(RowData + " This row is not exist.");
		}
		return -1;
	}

	public int ColumnValue(String ColumnName, String shName) {

		sh = wb.getSheet(shName);
		int cnt = sh.getRow(0).getLastCellNum();
		for (int j = 0; j <= cnt; j++) {

			String Value = sh.getRow(0).getCell(j).getStringCellValue().trim();
			if (ColumnName.trim().equals(Value.trim())) {
				Exist = true;
				return j;
			}
		}
		if (!Exist) {
			System.out.println(ColumnName + " This column is not exist.");
		}

		return -1;
	}

	public ExtentReports getExtent() {
		return extent;
	}

	public void setExtent(ExtentReports extent) {
		this.extent = extent;
	}

	// wait till particular element is visible upto certain seconds
	public boolean fnWaitForVisibility(WebElement element, int waitFor, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, waitFor);
		wait.until(ExpectedConditions.visibilityOf(element));
		return element.isDisplayed();
	}

	//wait till element is visible for 3 sec
	public boolean fnWaitForVisibility(WebElement element, int timer ) {
		WebDriverWait wait = new WebDriverWait(driver, timer);
		wait.until(ExpectedConditions.visibilityOf(element));
		return element.isDisplayed();
	}

	// to delay the thread for certain amount of time in milisec
	public void Sleep(int wait) {
		try {
			Thread.sleep(wait);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


// for taking the screenshot
	public void fnTakeScreenAshot(ConfigTestRunner configTestRunner, String status, String Message,
			String screenShotName) {

		if (status.equalsIgnoreCase("Pass")) {
			try {
				configTestRunner.getChildTest().log(Status.PASS, Message + " " + configTestRunner.getChildTest()
						.addScreenCaptureFromPath(configTestRunner.screenShotName(screenShotName)));

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (status.equalsIgnoreCase("Fail")) {
			try {
				configTestRunner.getChildTest().log(Status.FAIL, Message + " " + configTestRunner.getChildTest()
						.addScreenCaptureFromPath(configTestRunner.screenShotName(screenShotName)));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void executeExtJsSetText( WebElement element,String value) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].value='"+value+"';", element);

    }

    // use it inputbox loctor and value from excel
	public void setFocusEnterSpanInput(WebElement element, WebElement elementinput, String value) {
        Actions action = new Actions(driver);
        action.moveToElement(element);
        action.click();
        action.build().perform();
        sleep(1000);
        Actions action1 = new Actions(driver);
        action1.moveToElement(elementinput);
        action1.sendKeys(value);
        action1.sendKeys(Keys.DOWN);
        action1.sendKeys(Keys.ENTER);
        action1.build().perform();
    }

	// move to particular element then click
	public void moveToClickElement(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.click();
        actions.build().perform();
    }
	
	 // use it inputbox loctor and value from excel
    public void setFocusEnter(WebElement element, String value) {
        Actions action = new Actions(driver);
        action.moveToElement(element);
        action.sendKeys(value);
        action.sendKeys(Keys.DOWN);
        action.sendKeys(Keys.ENTER);
        action.build().perform();
    }
	
	public Map<String, String> getTestCase() {
		return testCase;
	}

	public void setTestCase(Map<String, String> testCase) {
		this.testCase = testCase;
	}

	public Map<String, String> getTestData() {
		return testData;
	}

	public void setTestData(Map<String, String> testData) {
		this.testData = testData;
	}

	// to select particular webelement with xpath from the .properties file
	public WebElement getWebElement(String name, String formName, ConfigTestRunner configTestRunner) {
		return configTestRunner.getConfig().getWebElement(configTestRunner.driver, formName, name);
	}

	// to select a dropdown to select the name
	public Select getSelectWebElement(String name, String formName, ConfigTestRunner configTestRunner){
		WebElement selectObj = configTestRunner.getConfig().getWebElement(configTestRunner.driver, formName, name);
		Select selectObjDD = new Select(selectObj);
		return selectObjDD;
		
	}
	
	public Select getSelectWebElementPOM(WebElement selectObj) {
		Select selectObjDD = new Select(selectObj);
		return selectObjDD;
	}

	//to select multiple elements with same xpath , tag etc.
	public List<WebElement> getWebElements(ConfigTestRunner configTestRunner, String formName, String name) {
		List<WebElement> webElement = configTestRunner.getConfig().getWebElementes(configTestRunner.driver, formName,
				name);
		return webElement;
	}

	// select an element for xpath only
	public WebElement getElement(String xpath) {
		return driver.findElement(By.xpath(xpath));
	}

	//
	public void sleep(int timer) {
		try {
			Thread.sleep(timer);
		} catch (Exception e) {

		}
	}

	//
	public void listElementClickFirst(String xpath) {
		int count =driver.findElements(By.xpath(xpath)).size();
		if(count > 1) {
			List<WebElement> li = driver.findElements(By.xpath(xpath));
			li.get(0).click();
		}else {
			driver.findElement(By.xpath(xpath)).click();
		}
	}

	// wait for a particular time then click on the element
	public void waitAndClick(final WebElement element, int waitfor, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, waitfor);
		wait.until(new ExpectedCondition<WebElement>() {
			public ExpectedCondition<WebElement> visibilityOfElement = ExpectedConditions.visibilityOf(element);

			@Override
			public WebElement apply(WebDriver driver) {
				try {
					WebElement elementx = this.visibilityOfElement.apply(driver);
					if (elementx == null) {
						return null;
					}
					if (elementx.isDisplayed() && elementx.isEnabled()) {
						elementx.click();
						return elementx;
					} else {
						return null;
					}
				} catch (WebDriverException e) {
					return null;
				}
			}
		});
	}

	public void setCellData(String Data, int row, int col) {

		DataFormatter formatter = new DataFormatter();
		String data1 = formatter.formatCellValue(sh.getRow(row).getCell(col));
		if (!data1.isEmpty())
			sh.getRow(row).getCell(col).setCellValue("");
		if (!(Data.isEmpty())) {
			sh.getRow(row).getCell(col).setCellValue(Data);

			try {
				FileOutputStream fileOut = new FileOutputStream(Constants.TestDataPath + "/TestData.xlsx");
				wb.write(fileOut);
				fileOut.flush();
				fileOut.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

}
